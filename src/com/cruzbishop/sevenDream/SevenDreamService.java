/*
 * Copyright (c) 2012 Cruz Julian Bishop
 */

package com.cruzbishop.sevenDream;

import android.graphics.Color;
import android.service.dreams.DreamService;
import android.widget.TextView;

/**
 * A dream service that draws a 7
 *
 * @since 1.0.0.0
 * @author Cruz Julian Bishop
 */
public class SevenDreamService extends DreamService {

    /**
     * Called when this {@link DreamService} is attached to the window
     *
     * @since 1.0.0.0
     */
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        //Allow interaction with this dream service
        setInteractive(true);

        //Ensure full-screen usage
        setFullscreen(true);

        //Set up a temporary TextView
        //TODO Replace this with an OpenGL or Canvas based view
        TextView view = new TextView(this);

        setContentView(view);

        view.setText("7");

        view.setTextSize(512);

        view.setTextColor(Color.RED);

        view.setBackground(getWallpaper());

        view.setShadowLayer(5, 5, 5, Color.BLACK);
    }
}
